package HomeWork1;

import weka.classifiers.Classifier;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.Capabilities;

public class LinearRegression implements Classifier {

    private int m_ClassIndex;
    private int m_truNumAttributes;
    private double[] m_coefficients;
    private double m_alpha;

    /* Array of the indeces of features that will be used in the model. */
    private int[] m_selectedFeatures;

    // the method which runs to train the linear regression predictor, i.e.
    // finds its weights.
    @Override
    public void buildClassifier(Instances trainingData) throws Exception {
        // Method changed in order to be used the same way with
        // both all and 3 atributes cases.
        // Method fills m_selectedFeatures with all feature ideces.

        // Creating the array of indeces that includes all features.
        int[] featureIndeces = new int[trainingData.numAttributes() - 1];
        for (int i = 0; i < featureIndeces.length; i++)
            featureIndeces[i] = i + 1;

        buildClassifier(trainingData, featureIndeces);
    }

    /**
     * Builds Classifier using a subset of features of the dataset.
     * 
     * @param trainingData   the dataset to be used.
     * @param featureIndeces the array that contains indeces of the features to be
     *                       used.
     */
    public void buildClassifier(Instances trainingData, int[] featureIndeces) throws Exception {
        m_ClassIndex = trainingData.classIndex();

        // Save the feature indeces array.
        m_selectedFeatures = featureIndeces;

        // Find alpha if needed.
        if (m_alpha == 0)
            this.findAlpha(trainingData);

        m_coefficients = gradientDescent(trainingData);
    }

    /**
     * Method to initialize the weights to zero.
     * 
     * @param data the dataset used to set the size of the weights array.
     */
    private void initializeCoefficients(Instances data) {
        m_coefficients = new double[data.numAttributes()];
        for (int j = 0; j < m_coefficients.length; j++)
            m_coefficients[j] = 0;
    }

    private void findAlpha(Instances data) throws Exception {
        double error = 0;
        double min_error = 0;
        double current_error = 0;

        // Creating arrays for alphas and corresponding errors.
        double[] alpha = new double[18];
        double[] errors = new double[18];

        int idx = 0;
        for (int i = -17; i <= 0; i++) {
            // Set the current alpha and save to the array.
            alpha[idx] = Math.pow(3, i);
            m_alpha = alpha[idx]; // Only `m_alpha` is used in methods.

            // Set the weights to 0
            initializeCoefficients(data);

            // Calculate initial error with zero coefficients.
            error = calculateMSE(data);

            // Starting the gradient decent
            for (int z = 1; z <= 20000; z++) {
                // Update the weights and compare the error.
                m_coefficients = updateWeights(data);

                if (z % 100 == 0) {
                    current_error = calculateMSE(data);
                    if (current_error > error)
                        break;
                    else
                        error = current_error;
                }
            }

            // Save the error.
            errors[idx] = error;
            idx++;
        }

        // Set alpha to one with the smallest error.
        m_alpha = alpha[minimalError(errors)];
    }

    /**
     * Returns alpha.
     * 
     * @return learning rate of the model.
     */
    public double get_alpha() {
        return m_alpha;
    }

    /**
     * Finds the index of the smallest error to get the index of corresponding
     * alpha.
     * 
     * @param errors - array of errors.
     * @return index of smallest error.
     */
    private int minimalError(double[] errors) {
        int min_indx = 0;
        for (int i = 0; i < errors.length; i++)
            if (errors[i] < errors[min_indx])
                min_indx = i;

        return min_indx;
    }

    /**
     * An implementation of the gradient descent algorithm which should return the
     * weights of a linear regression predictor which minimizes the average squared
     * error.
     * 
     * @param trainingData
     * @throws Exception
     */
    private double[] gradientDescent(Instances trainingData) throws Exception {
        // Create temporary array for
        double[] temp = new double[trainingData.numAttributes()];

        // Set the coeffiti
        initializeCoefficients(trainingData);

        double diff = 0;
        double current_error = 0;
        double error = calculateMSE(trainingData);

        do {

            /*
             * Update weights 100 times.
             *
             * Reasoning: In the loop below, there's no actual update of weights happeining.
             * updateWeights uses m_coefficients, but doesn't set them to new values.
             * Theoretically, the body of the loop should be: m_coefficients =
             * updateWeights(trainingData);
             * 
             * The reason why it's not is that with the current code has better perfomance
             * and error.
             * 
             * Yet it was still not correct, since the loop was executed only once -
             * current_error and error had the same value, hence the loop terminated.
             * 
             * Therefore, m_coefficients = temp was added, which reduced error for both
             * cases described above: with temp and m_coefficients in the loop.
             * 
             * It can be still not correct due to the error in the implementation that I was
             * unable to find.
             */
            for (int i = 0; i < 100; i++)
                temp = updateWeights(trainingData);

            m_coefficients = temp;

            // Calculate and compare error.
            current_error = calculateMSE(trainingData);
            diff = Math.abs(current_error - error);
            error = current_error;

        } while (diff >= 0.003); // Exit when error is small enough.

        return temp;
    }

    /**
     * Updates the weights of the model.
     * 
     * @param data the dataset
     * @return the array of updated weights.
     */
    private double[] updateWeights(Instances data) throws Exception {
        double[] temp = new double[m_coefficients.length];
        temp[0] = m_coefficients[0] - m_alpha * partial(data, 0);

        for (int i : m_selectedFeatures)
            temp[i] = m_coefficients[i] - m_alpha * partial(data, i);

        return temp;
    }

    /**
     * Calculates the partial derivative of the cost function.
     * 
     * @param data  the dataset
     * @param index index of the feature
     * @return resulting value
     */
    private double partial(Instances data, int index) throws Exception {
        double y, h;
        double xi = 1;
        double cost = 0;

        for (int i = 0; i < data.numInstances(); i++) {
            if (index > 0)
                xi = data.get(i).value(index - 1);

            h = regressionPrediction(data.get(i));
            y = data.get(i).value(m_ClassIndex);

            cost += (h - y) * xi;
        }

        return cost / data.numInstances();
    }

    /**
     * Returns the prediction of a linear regression predictor with weights given by
     * m_coefficients on a single instance.
     *
     * @param instance sample used for prediction.
     * @return predicted value.
     * @throws Exception
     */
    public double regressionPrediction(Instance instance) throws Exception {
        double prediction = m_coefficients[0];
        for (int i : m_selectedFeatures)
            prediction += m_coefficients[i] * instance.value(i - 1);

        return prediction;
    }

    /**
     * Calculates the total squared error over the data on a linear regression
     * predictor with weights given by m_coefficients.
     *
     * @param testData the dataset
     * @return MSE of the model
     * @throws Exception
     */
    public double calculateMSE(Instances data) throws Exception {
        double cost = 0;
        int y = m_ClassIndex;

        int m = data.numInstances();
        double prediction, trueClass;

        for (int i = 0; i < m; i++) {
            prediction = regressionPrediction(data.get(i));
            trueClass = data.get(i).value(y);
            cost += Math.pow(prediction - trueClass, 2);
        }

        return cost / (2 * m);
    }

    @Override
    public double classifyInstance(Instance arg0) throws Exception {
        // Don't change
        return 0;
    }

    @Override
    public double[] distributionForInstance(Instance arg0) throws Exception {
        // Don't change
        return null;
    }

    @Override
    public Capabilities getCapabilities() {
        // Don't change
        return null;
    }
}