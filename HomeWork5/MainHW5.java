package HomeWork5;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Random;

import weka.core.Instance;
import weka.core.Instances;
import weka.classifiers.functions.supportVector.Kernel;
import weka.classifiers.functions.supportVector.PolyKernel;
import weka.classifiers.functions.supportVector.RBFKernel;

public class MainHW5 {

	public static BufferedReader readDataFile(String filename) {
		BufferedReader inputReader = null;

		try {
			inputReader = new BufferedReader(new FileReader(filename));
		} catch (FileNotFoundException ex) {
			System.err.println("File not found: " + filename);
		}

		return inputReader;
	}

	public static Instances loadData(String fileName) throws IOException {
		BufferedReader datafile = readDataFile(fileName);
		Instances data = new Instances(datafile);
		data.setClassIndex(data.numAttributes() - 1);
		return data;
	}

	public static void main(String[] args) throws Exception {
		Instances data = loadData("cancer.txt");
		data.randomize(new Random(1515L));

		Instances train = data.trainCV(5, 4);
		Instances test = data.testCV(5, 4);

		SVM svm = new SVM();
		double[] polyDegrees = { 2d, 3d, 4d };
		double[] rbfGamma = { 0.005, 0.05, 0.5 };
		final boolean printBestKernel = true;

		PolyKernel poly = new PolyKernel();
		double[] polyErrors = new double[polyDegrees.length];

		int idx = 0;
		for (double degree : polyDegrees) {
			poly.setExponent(degree);
			svm.setKernel(poly);
			svm.buildClassifier(train);

			System.out.printf("For PolyKernel with degree %f the rates are:\n", degree);
			printRatios(svm, test, !printBestKernel);
			polyErrors[idx++] = calcMargin(svm, test);
		}

		RBFKernel rbf = new RBFKernel();
		double[] rbfErrors = new double[rbfGamma.length];

		idx = 0;
		for (double gamma : rbfGamma) {
			rbf.setGamma(gamma);
			svm.setKernel(rbf);
			svm.buildClassifier(train);

			System.out.printf("For RBFKernel with gamma %f the rates are:\n", gamma);
			printRatios(svm, test, !printBestKernel);
			rbfErrors[idx++] = calcMargin(svm, test);
		}

		int polyMax = findMaxErrorIndex(polyErrors);
		int rbfMax = findMaxErrorIndex(rbfErrors);

		if (polyErrors[polyMax] > rbfErrors[rbfMax]) {
			System.out.printf("Best Kernel is: Poly with degree %f, ", polyDegrees[polyMax]);

			poly.setExponent(polyDegrees[polyMax]);
			svm.setKernel(poly);
			svm.buildClassifier(train);

			printRatios(svm, test, printBestKernel);
		} else {
			System.out.printf("Best Kernel is: RBF with gamma %f, ", rbfGamma[rbfMax]);

			rbf.setGamma(rbfGamma[rbfMax]);
			svm.setKernel(rbf);
			svm.buildClassifier(train);

			printRatios(svm, test, printBestKernel);
		}

		int[] iValues = { 1, 0, -1, -2, -3, -4 };
		int[] jValues = { 3, 2, 1 };

		double c = 0d;
		for (int j = 0; j < jValues.length; j++)
			for (int i = 0; i < iValues.length; i++) {
				c = calcC(iValues[i], jValues[j]);
				svm.setC(c);
				svm.buildClassifier(train);

				System.out.printf("For C %f the rates are:\n", c);
				printRatios(svm, test, !printBestKernel);
			}

	}

	private static int findMaxErrorIndex(double[] errors) {
		int idx = 0;

		for (int i = 0; i < errors.length; i++)
			if (errors[i] > errors[idx])
				idx = i;

		return idx;
	}

	private static double calcC(double i, double j) {
		return (Math.pow(10, i) * j) / 3d;
	}

	private static double calcMargin(SVM svm, Instances data) throws Exception {
		int[] confusion = svm.calcConfusion(data);
		int tp = confusion[0], fp = confusion[1], tn = confusion[2], fn = confusion[3];

		double a = 1.5;
		double fpr = (double) fp / (fp + tn);
		double tpr = (double) tp / (tp + fn);

		return a * tpr - fpr;
	}

	private static void printRatios(SVM svm, Instances data, boolean inline) throws Exception {
		int[] confusion = svm.calcConfusion(data);
		int tp = confusion[0], fp = confusion[1], tn = confusion[2], fn = confusion[3];

		// System.out.printf("TP = %d, FP = %d, TN = %d, FN = %d\n", tp, fp, tn, fn);

		double fpr = (double) fp / (fp + tn);
		double tpr = (double) tp / (tp + fn);

		if (inline) {
			System.out.printf("%f / %f\n", tpr, fpr);
		} else {
			System.out.printf("TPR = %f\n", tpr);
			System.out.printf("FPR = %f\n", fpr);
			System.out.printf("Error = %f\n", calcMargin(svm, data));
		}

		System.out.println();
	}
}
