package HomeWork2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import weka.core.Instances;

public class MainHW2 {

	public static BufferedReader readDataFile(String filename) {
		BufferedReader inputReader = null;

		try {
			inputReader = new BufferedReader(new FileReader(filename));
		} catch (FileNotFoundException ex) {
			System.err.println("File not found: " + filename);
		}

		return inputReader;
	}

	/**
	 * Sets the class index as the last attribute.
	 * 
	 * @param fileName
	 * @return Instances data
	 * @throws IOException
	 */
	public static Instances loadData(String fileName) throws IOException {
		BufferedReader datafile = readDataFile(fileName);

		Instances data = new Instances(datafile);
		data.setClassIndex(data.numAttributes() - 1);
		return data;
	}

	public static void main(String[] args) throws Exception {
		Instances trainingCancer = loadData("cancer_train.txt");
		Instances testingCancer = loadData("cancer_test.txt");
		Instances validationCancer = loadData("cancer_validation.txt");

		DecisionTree tree = new DecisionTree();
		tree.findImpurityMeasure(trainingCancer, validationCancer);
		tree.buildClassifier(trainingCancer);

		System.out.println("Train error: " + tree.calcAverageError(trainingCancer));
		System.out.println("Test error:  " + tree.calcAverageError(testingCancer));

		double[] cutoff = { 0.75, 0.5, 0.25, 0.05, 0.005 };
		double[][] chiTable = { { 0.102, 0.575, 1.213, 1.923, 2.675, 3.455, 4.255, 5.071, 5.899, 6.739 },
				{ 0.455, 1.386, 2.366, 3.357, 4.351, 5.348, 6.346, 7.344, 8.343, 9.342 },
				{ 1.323, 2.773, 4.108, 5.385, 6.626, 7.841, 9.037, 10.219, 11.389, 12.549 },
				{ 3.841, 5.991, 7.815, 9.488, 11.070, 12.592, 14.067, 15.507, 16.919, 18.307 },
				{ 7.879, 10.597, 12.838, 14.860, 16.750, 18.548, 20.278, 21.955, 23.589, 25.188 } };

		printInfo(tree, 1.0, null, trainingCancer, validationCancer);

		double currentError = 0;
		double bestError = tree.calcAverageError(validationCancer);
		int bestErrorIdx = -1;

		for (int i = 0; i < cutoff.length; i++) {
			tree.setCutoff(chiTable[i]);
			tree.buildClassifier(trainingCancer);

			printInfo(tree, cutoff[i], chiTable[i], trainingCancer, validationCancer);

			currentError = tree.calcAverageError(validationCancer);
			if (currentError < bestError) {
				bestError = currentError;
				bestErrorIdx = i;
			}
		}

		if (bestErrorIdx >= 0)
			tree.setCutoff(chiTable[bestErrorIdx]);
		tree.buildClassifier(trainingCancer);

		System.out.printf("Best validation error at p_value = %f\n", bestError);
		System.out.printf("Test error with best tree: %f\n", tree.calcAverageError(testingCancer));
		tree.printTree();
	}

	private static void printInfo(DecisionTree tree, double p_value, double[] distribution, Instances train,
			Instances valid) throws Exception {

		tree.setCutoff(distribution);
		tree.buildClassifier(train);

		System.out.println("--------------------------------------------------");
		System.out.printf("Decision tree with p_value of: %f\n", p_value);
		System.out.printf("The train error of the decision tree is %f\n", tree.calcAverageError(train));
		System.out.printf("Max height on validation data: %d\n", tree.maxHeight(valid));
		System.out.printf("Average height on validation data: %f\n", tree.averageHeight(valid));
		System.out.printf("The validation error of the decision tree is %f\n", tree.calcAverageError(valid));
		System.out.println("--------------------------------------------------");

	}
}
