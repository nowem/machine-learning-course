package HomeWork3;

import weka.core.Instance;
import weka.core.Instances;

public class FeatureScaler {
	/**
	 * Returns a scaled version (using standarized normalization) of the given
	 * dataset.
	 * 
	 * @param instances The original dataset.
	 * @return A scaled instances object.
	 */
	public Instances scaleData(Instances instances) {
		Instance newX = null;
		Instances scaled = new Instances(instances, 0);

		double[] mean = new double[instances.numAttributes()];
		double[] std = new double[instances.numAttributes()];

		for (int i = 0; i < instances.numAttributes(); i++) {
			mean[i] = mean(instances, i);
			std[i] = std(instances, i);
		}

		for (int i = 0; i < instances.numInstances(); i++) {
			newX = filterInstance(instances.get(i), mean, std);
			scaled.add(newX);
		}

		return scaled;
	}

	public Instance filterInstance(Instance x, double[] mean, double[] std) {
		double[] newValues = new double[x.numAttributes()];
		for (int i = 0; i < x.numAttributes(); i++) {
			if (std[i] != -1 && i != x.numAttributes() - 1) {
				newValues[i] = (x.value(i) - mean[i]) / std[i];
			} else {
				newValues[i] = x.value(i);
			}
		}

		return x.copy(newValues);
	}

	private double std(Instances data, int attrIdx) {
		return data.attribute(attrIdx).isNumeric() ? Math.sqrt(data.variance(attrIdx)) : -1;
	}

	private double mean(Instances data, int attrIdx) {
		double sum = 0;
		for (int i = 0; i < data.numInstances(); i++)
			sum += data.get(i).value(attrIdx);

		return sum / data.numInstances();
	}
}