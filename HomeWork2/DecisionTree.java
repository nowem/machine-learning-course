package HomeWork2;

import weka.classifiers.Classifier;
import weka.core.Instance;
import weka.core.InstanceComparator;
import weka.core.Instances;
import weka.core.Capabilities;

import java.util.TreeSet;
import java.util.LinkedList;

class Node {
	Node[] children;
	Node parent;
	int attributeIndex;
	double returnValue;

	/* added fields */
	Instances subset;
	double splitValue;
}

/** Enum for impurity measure. */
enum Impurity {
	Entropy, Gini
}

public class DecisionTree implements Classifier {

	private Node rootNode;
	private Impurity impurityMeasure;
	private double[] cutoff;

	@Override
	public void buildClassifier(Instances arg0) throws Exception {
		// Stop building if impurity measure is not found
		if (this.impurityMeasure == null) {
			System.out.println("Impurity measure is not set");
			return;
		}

		buildTree(arg0, this.impurityMeasure);
	}

	@Override
	public double classifyInstance(Instance instance) {
		int idx = 0;
		double testValue = 0d;
		Node n = this.rootNode;
		while (n.attributeIndex != -1 && n.children != null) {
			testValue = instance.value(n.attributeIndex);
			idx = findIndx(n.children, testValue);

			if (idx < 0)
				break;
			n = n.children[idx];
		}

		return n.returnValue;
	}

	/**
	 * Calculates the height of the tree used to classify an instance.
	 * 
	 * @param inst - instance to be classified.
	 * @return height
	 */
	private int classificationHeight(Instance inst) {
		int idx = 0;
		double testValue = 0d;
		Node n = this.rootNode;
		int height = 0;

		while (n.attributeIndex != -1 && n.children != null) {
			testValue = inst.value(n.attributeIndex);
			idx = findIndx(n.children, testValue);

			if (idx < 0)
				break;
			n = n.children[idx];
			height++;
		}

		return height;
	}

	/**
	 * Calculates average height of the tree.
	 * 
	 * @param data - tested dataset
	 * @return average height
	 */
	public double averageHeight(Instances data) {
		int heightSum = 0;
		for (int i = 0; i < data.numInstances(); i++)
			heightSum += classificationHeight(data.get(i));

		return (double) heightSum / data.numInstances();
	}

	/**
	 * Calculates maximal height of the tree.
	 * 
	 * @param data - tested dataset
	 * @return maximal height
	 */
	public int maxHeight(Instances data) {
		int maxHeight = 0;
		int currentHeight = 0;
		for (int i = 0; i < data.numInstances(); i++) {
			currentHeight = classificationHeight(data.get(i));
			if (currentHeight > maxHeight)
				maxHeight = currentHeight;
		}

		return maxHeight;
	}

	/**
	 * Sets relevant Chi Square Table distribution.
	 * 
	 * @param distribution - distribution for desired p-value.
	 */
	public void setCutoff(double[] distribution) {
		this.cutoff = distribution;
	}

	/**
	 * Finds index of the child node.
	 * 
	 * @param children - array of children nodes
	 * @param value    - desired split value
	 * @return index of the child node in the array.
	 */
	private int findIndx(Node[] children, double value) {
		for (int i = 0; i < children.length; i++) {
			if (children[i] != null && children[i].splitValue == value)
				return i;
		}

		return -1;
	}

	@Override
	public double[] distributionForInstance(Instance arg0) throws Exception {
		// Don't change
		return null;
	}

	/**
	 * Finds best impurity measure
	 * 
	 * @param train      - training dataset used to build the decision tree
	 * @param validation - dataset used to calculate average error of the tree
	 */
	public void findImpurityMeasure(Instances train, Instances validation) throws Exception {
		double giniError = 0;
		double entropyError = 0;

		buildTree(train, Impurity.Gini);
		giniError = calcAverageError(validation);
		System.out.println("Validation error using Gini: " + giniError);

		buildTree(train, Impurity.Entropy);
		entropyError = calcAverageError(validation);
		System.out.println("Validation error using Entropy: " + entropyError);

		this.impurityMeasure = giniError < entropyError ? Impurity.Gini : Impurity.Entropy;
	}

	/**
	 * Constructs the decision tree
	 * 
	 * @param data    - the original dataset
	 * @param measure - impurity measure
	 */
	public void buildTree(Instances data, Impurity measure) throws Exception {
		// Create new root node
		this.rootNode = new Node();
		this.rootNode.parent = null;
		this.rootNode.subset = data;
		this.rootNode.returnValue = findNodeReturnValue(data);

		// Collect attribute indices
		LinkedList<Integer> attributes = new LinkedList<>();
		for (int i = 0; i < data.numAttributes() - 1; i++) {
			attributes.add(i);
		}

		// Create a queue with only root node
		LinkedList<Node> queue = new LinkedList<>();
		queue.add(rootNode);

		Node n = null;
		int maxGainAttrIdx = 0;
		double[] attrValues = null;

		// Repeat until there are node in the queue
		while ((n = queue.poll()) != null) {
			// If node is perfectly classified or there are no attributes left
			// set this node as a leaf
			if (perfectlyClassified(n.subset) || attributes.isEmpty()) {
				n.attributeIndex = -1;
				n.children = null;
				n.returnValue = findNodeReturnValue(n.subset);
				prune(n, this.cutoff);
				continue;
			}

			// Find attribute with maximal information gain accroding to impurity measure
			maxGainAttrIdx = findMaximalGain(n, attributes, measure);

			// Remove the attribute from the list
			attributes.remove((Object) maxGainAttrIdx);
			// Find values of the attribute
			attrValues = findAttrValues(n.subset, maxGainAttrIdx);

			// Set node fields
			n.attributeIndex = maxGainAttrIdx;
			n.children = new Node[attrValues.length];
			n.returnValue = findNodeReturnValue(n.subset);

			// Create children nodes and give them values
			for (int i = 0; i < attrValues.length; i++) {
				n.children[i] = new Node();
				n.children[i].parent = n;
				// Set the split value
				n.children[i].splitValue = attrValues[i];
				// Construct the subset of the parent dataset
				n.children[i].subset = consturctSubset(n.subset, n.attributeIndex, attrValues[i]);
				// Add node to the queue
				queue.add(n.children[i]);
			}
		}
	}

	/**
	 * Prunes the decision tree according to given p-value.
	 * 
	 * @param n      - starting node
	 * @param cutoff - relevant Chi Square distribution.
	 */
	private void prune(Node n, double[] cutoff) {
		if (cutoff == null || n == rootNode)
			return;

		Node parent = n.parent;
		double chi = calcChiSquare(parent.subset, parent.attributeIndex);
		int df = findAttrValues(parent.subset, parent.attributeIndex).length - 2;
		int childIdx = 0;

		if (/* df > 0 && */ chi >= cutoff[df]) {
			childIdx = findChild(parent, n);
			if (childIdx >= 0) {
				parent.children[childIdx] = null;
				// n.parent = null;
			}

		}

		prune(parent, cutoff);
	}

	/**
	 * Finds child index in parent's children array.
	 * 
	 * @param parent - parent node
	 * @param child  - child node to be found
	 * @return index of the child in the array
	 */
	private int findChild(Node parent, Node child) {
		for (int i = 0; i < parent.children.length; i++)
			if (parent.children[i] != null && parent.children[i].equals(child))
				return i;

		return -1;
	}

	/**
	 * Detemines if the dataset on the node is perfectly classified
	 * 
	 * @param data - subset of the data on the node
	 * @return true if the dataset is perfectly classified
	 */
	private boolean perfectlyClassified(Instances data) {
		// Get the class label on the first instance in dataset
		double classValue = data.get(0).value(data.classIndex());

		// Check if all other instances have the same label
		for (int i = 0; i < data.numInstances(); i++) {
			if (data.get(i).value(data.classIndex()) != classValue)
				return false;
		}

		return true;
	}

	/**
	 * Finds node return value according to majority of the labels
	 * 
	 * @param data - the dataset of the node
	 * @return 0 or 1 depending on majority of the class labels
	 */
	private double findNodeReturnValue(Instances data) {
		int y = data.classIndex();
		int positiveClassification = 0;
		for (int i = 0; i < data.numInstances(); i++) {
			if (data.get(i).value(y) == 1.0)
				positiveClassification++;
		}

		return positiveClassification > (int) (data.numInstances() / 2) ? 1.0 : 0.0;
	}

	/**
	 * Finds maximal gain attribute with selected measure
	 * 
	 * @param n          - node
	 * @param attributes - list of available attributes
	 * @param measure    - selected impurity measure
	 * 
	 * @return index of the attribute with maximal gain
	 */
	private int findMaximalGain(Node n, LinkedList<Integer> attributes, Impurity measure) {
		double gain = 0.0;
		int maxGainIdx = 0;
		double maxGain = 0.0;

		for (int i : attributes) {
			gain = calcGain(n.subset, i, measure);
			if (gain > maxGain) {
				maxGain = gain;
				maxGainIdx = i;
			}
		}

		return maxGainIdx;
	}

	/**
	 * Prints the decision tree
	 * 
	 * @param data - the dataset of the tree
	 */
	public void printTree() {
		System.out.printf("Root\nReturn value: %f\n", rootNode.returnValue);
		printChildren(rootNode, 1);
	}

	/**
	 * Recursively prints children of the given node
	 * 
	 * @param data      - the dataset of the tree
	 * @param n         - parent node
	 * @param numOfTabs - number of tabs used for indentations on this level of the
	 *                  tree
	 */
	private void printChildren(Node n, int numOfTabs) {
		if (n.children == null)
			return;

		// Define the formats for internal nodes and leaves
		String nodeFormat = "%sif attribute %d = %.2f\n%sReturningValue: %.2f\n";
		String leafFormat = "%s\n%sReturningValue: %.2f\n";
		// Find number of tabs
		String tabs = nTabs(numOfTabs);

		// Print the children of the given node
		for (Node child : n.children) {

			if (child == null)
				continue;

			// If child is a leaf
			if (child.attributeIndex == -1) {

				System.out.printf(nodeFormat, tabs, n.attributeIndex, child.splitValue, tabs + "\tLeaf. ",
						child.returnValue);

			} else { // If child is internal node

				System.out.printf(nodeFormat, tabs, n.attributeIndex, child.splitValue, tabs, child.returnValue);

				// Continue to the next level
				printChildren(child, numOfTabs + 1);
			}
		}
	}

	/**
	 * Constructs the string with given number of tabs
	 * 
	 * @param n - desired number of tabs
	 * @return string that contains n number of tabs
	 */
	public String nTabs(int n) {
		StringBuilder tabs = new StringBuilder();
		for (int i = 0; i < n; i++)
			tabs.append("\t");

		return tabs.toString();
	}

	/**
	 * Calculates average error on given dataset
	 * 
	 * @param data - the dataset
	 * @return average error
	 */
	public double calcAverageError(Instances data) {
		double trueClass = 0;
		double prediction = 0;

		double totalError = 0;
		for (int i = 0; i < data.numInstances(); i++) {
			prediction = classifyInstance(data.get(i));
			trueClass = data.get(i).value(data.classIndex());
			totalError += Math.abs(prediction - trueClass);
		}

		return totalError / data.numInstances();
	}

	/**
	 * Calculates gain on the given attribute and selected impurity measure
	 * 
	 * @param data    - the dataset
	 * @param attrIdx - the attribute index
	 * @param measure - selected impurity measure
	 * 
	 * @return the gain on given attribute
	 */
	public double calcGain(Instances data, int attrIdx, Impurity measure) {
		switch (measure) {
		default:
		case Entropy:
			return informationGain(data, attrIdx);
		case Gini:
			return giniGain(data, attrIdx);
		}
	}

	/**
	 * Calculates Gini gain on given attribute
	 * 
	 * @param data    - the dataset
	 * @param attrIdx - given attribute index
	 * 
	 * @return Gini gain on given attribute
	 */
	public double giniGain(Instances data, int attrIdx) {
		// Collect values on given attribute
		double[] values = findAttrValues(data, attrIdx);
		// Calculate Gini before split
		double beforeSplit = calcGini(getProbabilities(data, values, attrIdx));

		double afterSplit = 0;
		double subsetGini = 0;
		// Create array to hold subsets after split
		Instances[] S = new Instances[values.length];
		for (int i = 0; i < values.length; i++) {
			// Construct subset for given value
			S[i] = consturctSubset(data, attrIdx, values[i]);
			// Calcuate Gini on the subset
			subsetGini = calcGini(getProbabilities(S[i], values, attrIdx));
			afterSplit += subsetGini * (S[i].numInstances() / data.numInstances());
		}

		return beforeSplit - afterSplit;
	}

	/**
	 * Calculates Gini for given set of probabilities
	 * 
	 * @param probabilities - the array of probabilities
	 * @return Gini impurity
	 */
	private double calcGini(double[] probabilities) {
		double probSum = 0;
		for (double prob : probabilities)
			probSum += Math.pow(prob, 2);

		return 1 - probSum;
	}

	/**
	 * Calculates information gain using entropy on given attribute
	 * 
	 * @param data    - the dataset
	 * @param attrIdx - given attribute index
	 * 
	 * @return information gain on given attribute
	 */
	public double informationGain(Instances data, int attrIdx) {
		// Collect the attribute values
		double[] values = findAttrValues(data, attrIdx);
		// Collect class values of the dataset
		double[] classValues = findAttrValues(data, data.classIndex());

		// Calculate the entropy before the split
		double beforeSplit = calcEntropy(getProbabilities(data, classValues, data.classIndex()));
		double afterSplit = 0;
		double subsetEntropy = 0;

		double totalInst = (double) data.numInstances();
		Instances[] S = new Instances[values.length];

		for (int i = 0; i < values.length; i++) {
			// Construct the subset for given value
			S[i] = consturctSubset(data, attrIdx, values[i]);
			// Calcuate the entropy for the subset
			subsetEntropy = calcEntropy(getProbabilities(S[i], classValues, data.classIndex()));
			afterSplit += subsetEntropy * (S[i].numInstances() / totalInst);
		}

		return beforeSplit - afterSplit;
	}

	/**
	 * Calculates entropy
	 * 
	 * @param probabilites - the array of probabilities
	 * @return entropy
	 */
	public double calcEntropy(double[] probabilities) {
		int numClasses = probabilities.length;
		double logCl = Math.log(numClasses);

		double entropy = 0;
		for (double p : probabilities) {
			if (p != 0)
				entropy += p * Math.log(p);
		}

		return -entropy;
	}

	/**
	 * Calculates Chi Square value for given attribute
	 * 
	 * @param data    - the dataset
	 * @param attrIdx - given attribute index
	 * 
	 * @return Chi Square value
	 */
	public double calcChiSquare(Instances data, int attrIdx) {
		double[] values = findAttrValues(data, attrIdx);

		double df = 0;

		int pf = 0;
		int nf = 0;

		double e0 = 0;
		double e1 = 0;

		double chi = 0;
		for (int i = 0; i < values.length; i++) {
			df = countInstances(data, attrIdx, values[i]);

			pf = countInstances(data, attrIdx, values[i], 0.0);
			nf = countInstances(data, attrIdx, values[i], 1.0);

			e0 = df * findProportion(data, 0.0, data.classIndex());
			e1 = df * findProportion(data, 1.0, data.classIndex());

			chi += (Math.pow(pf - e0, 2) / e0) + (Math.pow(nf - e1, 2) / e1);
		}

		return chi;
	}

	/**
	 * Counts instances with given value on selected attribute
	 * 
	 * @param data    - the dataset
	 * @param attrIdx - selected attribute
	 * @param value   - required value
	 * 
	 * @return number of instances with given value
	 */
	private int countInstances(Instances data, int attrIdx, double value) {
		int count = 0;
		for (int i = 0; i < data.numInstances(); i++)
			count += (data.get(i).value(attrIdx) == value) ? 1 : 0;

		return count;
	}

	/**
	 * Calculates instances with given value on selected attribute that are
	 * classified 1 or 0
	 * 
	 * @param data       - the dataset
	 * @param attrIdx    - given attribute index
	 * @param value      - required value
	 * @param classValue - requierd class label
	 * 
	 * @return number of instances with given value with the same class label
	 */
	private int countInstances(Instances data, int attrIdx, double value, double classLabel) {
		int count = 0;
		double xi = 0;
		double label = 0;
		for (int i = 0; i < data.numInstances(); i++) {
			xi = data.get(i).value(attrIdx);
			label = data.get(i).value(data.classIndex());
			count += (xi == value && label == classLabel) ? 1 : 0;
		}

		return count;
	}

	/**
	 * Constructs the subset of the data with given value on selected attribute
	 * 
	 * @param source  - the dataset
	 * @param attrIdx - given attribute index
	 * @param value   - desired value
	 * 
	 * @return a dataset that contains instances with given value on the attribute
	 */
	private Instances consturctSubset(Instances source, int attrIdx, double value) {
		Instances subset = new Instances(source, 0);
		for (int i = 0; i < source.numInstances(); i++)
			if (source.get(i).value(attrIdx) == value)
				subset.add(source.get(i));

		return subset;
	}

	/**
	 * Finds set of attribute values
	 * 
	 * @param data - the dataset
	 * @param idx  - index of the attribute
	 * 
	 * @return array of the attribute values
	 */
	private double[] findAttrValues(Instances data, int idx) {
		// Go the lazy way to collect unique values
		TreeSet<Double> attrValues = new TreeSet<>();
		for (int i = 0; i < data.numInstances(); i++)
			attrValues.add(data.get(i).value(idx));

		return collectValues(attrValues); // Convert values from TreeSet to the array
	}

	/**
	 * Converts attribute values from TreeSet to array
	 * 
	 * @param attrValues - TreeSet of attribute values
	 */
	private double[] collectValues(TreeSet<Double> attrValues) {
		double[] values = new double[attrValues.size()];

		int idx = 0;
		for (double value : attrValues)
			values[idx++] = value;

		return values;
	}

	/**
	 * Finds probabilities of the values on given attribute.
	 * 
	 * @param data    - the dataset
	 * @param values  - array of attribute values
	 * @param attrIdx - attribute index
	 * 
	 * @return array of probabilities
	 */
	public double[] getProbabilities(Instances data, double[] values, int attrIdx) {
		double[] probs = new double[values.length];

		int idx = 0;
		for (double value : values)
			probs[idx++] = findProportion(data, value, attrIdx);

		return probs;
	}

	/**
	 * Finds proportion of the value in the attribute
	 * 
	 * @param data  - the dataset
	 * @param value - required value
	 * @param idx   - index of the attribute
	 * 
	 * @return probability (proportion) of the instances with given value to total
	 *         number of instances
	 */
	private double findProportion(Instances data, double value, int idx) {
		return (double) countInstances(data, idx, value) / data.numInstances();
	}

	@Override
	public Capabilities getCapabilities() {
		// Don't change
		return null;
	}
}