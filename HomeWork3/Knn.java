package HomeWork3;

import weka.classifiers.Classifier;
import weka.core.Capabilities;
import weka.core.Instance;
import weka.core.Instances;

import java.util.ArrayList;
import java.util.Random;

class DistanceCalculator {
    public int p;
    public double threshold = Double.POSITIVE_INFINITY;

    /**
     * We leave it up to you wheter you want the distance method to get all relevant
     * parameters(lp, efficient, etc..) or have it has a class variables.
     */
    public double distance(Instance one, Instance two, Knn.DistanceCheck check) {
        switch (check) {
        default:
        case Regular:
            return this.p == 4 ? lInfinityDistance(one, two) : lpDisatnce(one, two);

        case Efficient:
            return this.p == 4 ? efficientLInfinityDistance(one, two) : efficientLpDisatnce(one, two);
        }
    }

    /**
     * Returns the Lp distance between 2 instances.
     * 
     * @param one
     * @param two
     */
    private double lpDisatnce(Instance one, Instance two) {
        double sum = 0;
        for (int i = 0; i < one.numAttributes() - 1; i++)
            sum += Math.pow(Math.abs(one.value(i) - two.value(i)), p);

        return Math.pow(sum, 1d / (double) p);
    }

    /**
     * Returns the L infinity distance between 2 instances.
     * 
     * @param one
     * @param two
     * @return
     */
    private double lInfinityDistance(Instance one, Instance two) {
        double max = 0, abs = 0;
        for (int i = 0; i < one.numAttributes() - 1; i++) {
            abs = Math.abs(one.value(i) - two.value(i));
            if (abs > max)
                max = abs;
        }

        return max;
    }

    /**
     * Returns the Lp distance between 2 instances, while using an efficient
     * distance check.
     * 
     * @param one
     * @param two
     * @return
     */
    private double efficientLpDisatnce(Instance one, Instance two) {
        double sum = 0;
        double stop = Math.pow(threshold, p);

        for (int i = 0; i < one.numAttributes() - 1; i++) {
            sum += Math.pow(Math.abs(one.value(i) - two.value(i)), p);
            if (sum > stop)
                return threshold;
        }

        return Math.pow(sum, 1d / (double) p);
    }

    /**
     * Returns the Lp distance between 2 instances, while using an efficient
     * distance check.
     * 
     * @param one
     * @param two
     * @return
     */
    private double efficientLInfinityDistance(Instance one, Instance two) {
        double max = 0;
        double abs = 0;
        for (int i = 0; i < one.numAttributes() - 1; i++) {
            abs = Math.abs(one.value(i) - two.value(i));
            if (max > threshold)
                return threshold;

            if (abs > max)
                max = abs;
        }

        return max;
    }
}

public class Knn implements Classifier {

    public enum DistanceCheck {
        Regular, Efficient
    }

    public enum WeightingScheme {
        Uniform, Weighted
    }

    public int m_kNeighbours;
    public int m_distance;
    public WeightingScheme m_weighting;
    public DistanceCheck m_distCheck;

    private Instances m_trainingInstances;
    private DistanceCalculator m_distCalc;
    private double[] m_knn = null;

    @Override
    /**
     * Build the knn classifier. In our case, simply stores the given instances for
     * later use in the prediction.
     * 
     * @param instances
     */
    public void buildClassifier(Instances instances) throws Exception {
        this.m_trainingInstances = instances;
        this.m_trainingInstances.randomize(new Random());

        this.m_distCalc = new DistanceCalculator();
    }

    /**
     * Returns the knn prediction on the given instance.
     * 
     * @param instance
     * @return The instance predicted value.
     */
    public double regressionPrediction(Instance instance, WeightingScheme scheme) {
        ArrayList<Instance> knn = findNearestNeighbors(instance);
        m_knn = new double[m_kNeighbours];
        for (int i = 0; i < m_knn.length; i++)
            m_knn[i] = m_distCalc.distance(instance, knn.get(i), m_distCheck);

        double value = 0;

        switch (scheme) {
        case Uniform:
            value = getAverageValue(knn, instance);
            break;
        case Weighted:
            value = getWeightedAverageValue(knn, instance);
            break;
        }

        return value;
    }

    /**
     * Caclcualtes the average error on a give set of instances. The average error
     * is the average absolute error between the target value and the predicted
     * value across all insatnces.
     * 
     * @param insatnces
     * @return
     */
    public double calcAvgError(Instances instances, WeightingScheme scheme) {
        double error = 0;
        int classIndex = m_trainingInstances.numAttributes() - 1;

        double trueValue = 0;
        double prediction = 0;

        for (int i = 0; i < instances.numInstances(); i++) {
            prediction = regressionPrediction(instances.get(i), scheme);
            trueValue = instances.get(i).value(classIndex);
            error += Math.abs(prediction - trueValue);
        }

        return error / instances.numInstances();
    }

    /**
     * Calculates the cross validation error, the average error on all folds.
     * 
     * @param insances     Insances used for the cross validation
     * @param num_of_folds The number of folds to use.
     * @return The cross validation error.
     */
    public double crossValidationError(Instances instances, int num_of_folds) {
        int subsetSize = (int) (instances.numInstances() / num_of_folds);
        Instances[] X = new Instances[num_of_folds];

        int startIndex = 0;
        int toCopy = 0;

        for (int i = 0; i < X.length; i++) {
            startIndex = i * subsetSize;
            toCopy = (i == X.length - 1) ? (instances.numInstances() - startIndex) : subsetSize;
            X[i] = new Instances(instances, startIndex, toCopy);
        }

        double error = 0;
        for (int i = 0; i < X.length; i++) {
            m_trainingInstances = constructTrainingSet(X, i);
            error += calcAvgError(X[i], m_weighting);
        }

        m_trainingInstances = instances;
        return error / num_of_folds;
    }

    /**
     * Constructs training dataset from subsets created in crossvalidation.
     * 
     * @param X               - array of subsets of the dataset
     * @param validationIndex - index of the validation set in the array of subsets
     * 
     * @return training set composed from N - 1 subsets
     */
    private Instances constructTrainingSet(Instances[] X, int validationIndex) {
        Instances train = new Instances(X[0], 0);

        for (int i = 0; i < X.length; i++) {
            if (i == validationIndex)
                continue;

            for (Instance x : X[i])
                train.add(x);
        }

        return train;
    }

    /**
     * Finds the k nearest neighbors.
     * 
     * @param instance
     */
    public ArrayList<Instance> findNearestNeighbors(Instance instance) {
        ArrayList<Instance> knn = new ArrayList<>(m_kNeighbours);
        m_distCalc.p = m_distance;

        int idx = 0;
        double distance = 0;
        Instance temp = null;
        for (int i = 0; i < m_trainingInstances.numInstances(); i++) {
            temp = m_trainingInstances.get(i);
            if (instance.equals(temp))
                continue;

            if (m_distCheck == DistanceCheck.Efficient && knn.size() == m_kNeighbours)
                m_distCalc.threshold = findThreshhold(m_knn);

            distance = m_distCalc.distance(instance, temp, m_distCheck);
            if (distance == Double.POSITIVE_INFINITY)
                continue;

            if (knn.size() < m_kNeighbours) {
                knn.add(temp);
            } else if ((idx = findNeighboursIndex(knn, distance, instance)) > 0) {
                knn.remove(idx);
                knn.add(temp);
            }
        }

        return knn;
    }

    /**
     * Finds neighbour index in the set of KNN that will be substituted by new
     * neighbour
     * 
     * @param knn      - set of KNN
     * @param distance - distance of the new KNN candidate
     * @param tested   - instance to be predicted.
     * 
     * @return index of the neighbour to be substitued or -1 if samples in KNN have
     *         smaller distance
     */
    private int findNeighboursIndex(ArrayList<Instance> knn, double distance, Instance tested) {
        for (int i = 0; i < knn.size(); i++) {
            if (distance < m_distCalc.distance(knn.get(i), tested, m_distCheck))
                return i;
        }

        return -1;
    }

    /**
     * Finds threshold to be used in efficient lp distance.
     * 
     * @param knn - set of KNN
     * 
     * @return threshold
     */
    private double findThreshhold(double[] knn) {
        if (knn == null)
            return Double.POSITIVE_INFINITY;

        double threshhold = 0;
        for (double d : knn)
            if (d > threshhold)
                threshhold = d;

        return threshhold;
    }

    /**
     * Cacluates the average value of the given elements in the collection.
     * 
     * @param
     * @return
     */
    public double getAverageValue(ArrayList<Instance> knn, Instance x) {
        double sum = 0;
        int classIndex = m_trainingInstances.numAttributes() - 1;

        for (Instance neighbor : knn) {
            if (m_distCalc.distance(x, neighbor, m_distCheck) == 0)
                return neighbor.value(classIndex);

            sum += neighbor.value(classIndex);
        }

        return sum / m_kNeighbours;
    }

    /**
     * Calculates the weighted average of the target values of all the elements in
     * the collection with respect to their distance from a specific instance.
     * 
     * @return
     */
    public double getWeightedAverageValue(ArrayList<Instance> knn, Instance x) {
        double w = 0;
        double sum = 0;
        double weightSum = 0;
        double distance = 0;

        int classIndex = m_trainingInstances.numAttributes() - 1;
        for (Instance neighbour : knn) {
            distance = m_distCalc.distance(x, neighbour, m_distCheck);
            if (distance == 0)
                return neighbour.value(neighbour.numAttributes() - 1);

            w = 1 / Math.pow(distance, 2);
            sum += w * neighbour.value(classIndex);
            weightSum += w;
        }

        // System.out.println("Sum: " + sum);
        // System.out.println(" W: " + weightSum);
        return sum / weightSum;
    }

    @Override
    public double[] distributionForInstance(Instance arg0) throws Exception {
        return null;
    }

    @Override
    public Capabilities getCapabilities() {
        return null;
    }

    @Override
    public double classifyInstance(Instance instance) {
        return 0.0;
    }
}
