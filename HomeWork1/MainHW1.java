package HomeWork1;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;

import java.util.HashSet;

public class MainHW1 {

	public static BufferedReader readDataFile(String filename) {
		BufferedReader inputReader = null;

		try {
			inputReader = new BufferedReader(new FileReader(filename));
		} catch (FileNotFoundException ex) {
			System.err.println("File not found: " + filename);
		}

		return inputReader;
	}

	/**
	 * Sets the class index as the last attribute.
	 * 
	 * @param fileName
	 * @return Instances data
	 * @throws IOException
	 */
	public static Instances loadData(String fileName) throws IOException {
		BufferedReader datafile = readDataFile(fileName);

		Instances data = new Instances(datafile);
		data.setClassIndex(data.numAttributes() - 1);
		return data;
	}

	public static void main(String[] args) throws Exception {
		// load data

		// Didn't know if we need to give files as arguments or
		// not, and didn't use Eclipse, so I'm not sure if this
		// is correct path.
		// While writing code, passed files as arguments,
		// so I keep the code in the comments if the
		// path at lines 61-62 is wrong.

		/*
		 * String train_file = args[0]; String test_file = args[1];
		 * 
		 * Instances train = loadData(train_file); Instances test = loadData(test_file);
		 */

		Instances train = loadData("wind_training.txt");
		Instances test = loadData("wind_testing.txt");

		// find best alpha and build classifier with all attributes
		LinearRegression gd = new LinearRegression();
		gd.buildClassifier(train);

		double allAttributesTrainErr = gd.calculateMSE(train);
		double allAttributesTestErr = gd.calculateMSE(test);

		System.out.println("The chosen alpha is: " + gd.get_alpha());
		System.out.println("Training error with all feature is: " + allAttributesTrainErr);
		System.out.println("Test error with all feature is: " + allAttributesTestErr);

		// build classifiers with all 3 attributes combinations

		// Find all combinations of 3 attributes
		int[][] featureSets = combinations(train);

		// Build classifier for each of 3 feature combinations and compute the error.
		double[] threeFeaturesErrors = new double[featureSets.length];
		for (int i = 0; i < featureSets.length; i++) {
			gd.buildClassifier(train, featureSets[i]);
			threeFeaturesErrors[i] = gd.calculateMSE(train);

			// Print the combinations and error
			printFeaturesWithError(train, featureSets[i], threeFeaturesErrors[i]);
		}

		// Find best 3 feature combination
		int[] bestThreeFeatures = featureSets[findBestFeaturesIndex(threeFeaturesErrors)];
		System.out.print("Best 3 features: ");
		for (int i : bestThreeFeatures)
			System.out.print(train.attribute(i - 1).name() + " ");
		System.out.println();

		// Build classifier using best 3 features and compute train and test error.
		gd.buildClassifier(train, bestThreeFeatures);
		System.out.println("Best 3 attributes train error: " + gd.calculateMSE(train));
		System.out.println("Best 3 attributes test error: " + gd.calculateMSE(test));
	}

	/**
	 * Find all unique combinations of 3 feature indeces using sets.
	 * 
	 * @param data the dataset
	 * @return the array of combinations
	 */
	private static int[][] combinations(Instances data) {
		int numAttr = data.numAttributes() - 1;
		HashSet<HashSet<Integer>> featureSets = new HashSet<>();
		HashSet<Integer> tmp;

		// Find combinations in lazy and dumb way.
		// HashSet will deal with copies.
		for (int i = 0; i < numAttr; i++)
			for (int j = 0; j < numAttr; j++)
				for (int k = 0; k < numAttr; k++) {
					if (i == j || j == k || i == k)
						continue;
					tmp = new HashSet<>();
					tmp.add(i + 1);
					tmp.add(j + 1);
					tmp.add(k + 1);

					featureSets.add(tmp);
				}

		return convertToArray(featureSets);
	}

	/**
	 * Convert HashSet that contains combinations to two dimensional array.
	 * 
	 * @param set set that contains combinations of 3 features.
	 * @return two dimentional array with combinations.
	 */
	private static int[][] convertToArray(HashSet<HashSet<Integer>> set) {
		int[][] converted = new int[set.size()][3];
		int idx = 0;
		int subidx = 0;
		for (HashSet<Integer> elem : set) {
			subidx = 0;
			for (int i : elem) {
				converted[idx][subidx] = i;
				subidx++;
			}

			idx++;
		}

		return converted;
	}

	/**
	 * Print attribute names with corresponding error.
	 * 
	 * @param data       the dataset containing the features and thier names.
	 * @param featureSet array with feature indeces.
	 * @param error      train error of the features
	 */
	private static void printFeaturesWithError(Instances data, int[] featureSet, double error) {
		for (int i : featureSet)
			System.out.print(data.attribute(i - 1).name() + " ");

		System.out.println(", error: " + error);
	}

	/**
	 * Find best combination of features index using error of the features.
	 * 
	 * @param featureErrors array that contains the train errors
	 * @return index of feature set with smallest error.
	 */
	private static int findBestFeaturesIndex(double[] featureErrors) {
		int minErrorIndex = 0;
		for (int i = 0; i < featureErrors.length; i++) {
			if (featureErrors[i] < featureErrors[minErrorIndex])
				minErrorIndex = i;
		}

		return minErrorIndex;
	}
}