package HomeWork3;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import HomeWork3.Knn.DistanceCheck;
import HomeWork3.Knn.WeightingScheme;
import weka.core.Instance;
import weka.core.Instances;

import java.util.Random;

public class MainHW3 {

	public static BufferedReader readDataFile(String filename) {
		BufferedReader inputReader = null;

		try {
			inputReader = new BufferedReader(new FileReader(filename));
		} catch (FileNotFoundException ex) {
			System.err.println("File not found: " + filename);
		}

		return inputReader;
	}

	public static Instances loadData(String fileName) throws IOException {
		BufferedReader datafile = readDataFile(fileName);
		Instances data = new Instances(datafile);
		data.setClassIndex(data.numAttributes() - 1);
		return data;
	}

	public static void main(String[] args) throws Exception {
		FeatureScaler fs = new FeatureScaler();
		Instances data = loadData("auto_price.txt");
		Instances scaled = fs.scaleData(data);

		Knn knn = new Knn();

		System.out.println("------------------------------");
		System.out.println("Results for original dataset:");
		System.out.println("------------------------------");
		hyperParameters(knn, data);

		System.out.println("------------------------------");
		System.out.println("Results for scaled dataset:");
		System.out.println("------------------------------");
		hyperParameters(knn, scaled);

		int[] folds = { data.numInstances(), 50, 10, 5, 3 };
		foldsAndStuff(knn, scaled, folds);
	}

	private static void foldsAndStuff(Knn knn, Instances data, int[] folds) throws Exception {
		knn.buildClassifier(data);

		double[] foldErrors = new double[folds.length];
		long start = 0, end = 0;
		long average = 0, total = 0;

		for (int i = 0; i < folds.length; i++) {
			System.out.println("------------------------------");
			System.out.printf("Results for %d folds:\n", folds[i]);
			System.out.println("------------------------------");
			knn.m_distCheck = DistanceCheck.Regular;

			start = System.nanoTime();
			foldErrors[i] = knn.crossValidationError(data, folds[i]);
			end = System.nanoTime();

			total = end - start;
			average = total / data.numInstances();
			System.out.printf(
					"Cross validation error regular knn for auto_price data is: %f and the average elapsed time is %d\n"
							+ "The total elapsed time is: %d\n\n",
					foldErrors[i], average, total);

			knn.m_distCheck = DistanceCheck.Efficient;
			start = System.nanoTime();
			foldErrors[i] = knn.crossValidationError(data, folds[i]);
			end = System.nanoTime();

			total = end - start;
			average = total / data.numInstances();
			System.out.printf(
					"Cross validation error efficient knn for auto_price data is: %f and the average elapsed time is %d\n"
							+ "The total elapsed time is: %d\n\n",
					foldErrors[i], average, total);
		}

	}

	private static int findPValue(double[][] arr) {
		double minError = findMinError(arr);

		for (int p = 0; p < arr.length; p++)
			for (int k = 0; k < arr[0].length; k++)
				if (arr[p][k] == minError)
					return p;

		return -1;
	}

	private static int findKValue(double[][] arr) {
		double minError = findMinError(arr);

		for (int p = 0; p < arr.length; p++)
			for (int k = 0; k < arr[0].length; k++)
				if (arr[p][k] == minError)
					return k;

		return -1;
	}

	private static double findMinError(double[][] arr) {
		double min = arr[0][0];
		for (double[] p : arr) {
			for (double d : p) {
				min = (min > d) ? d : min;
			}
		}

		return min;
	}

	private static void hyperParameters(Knn knn, Instances data) throws Exception {
		double[][] uniform = new double[4][20];
		double[][] weighted = new double[4][20];

		knn.buildClassifier(data);
		knn.m_distCheck = Knn.DistanceCheck.Regular;
		for (int i = 0; i < 4; i++) {
			knn.m_distance = i + 1;
			for (int j = 0; j < 20; j++) {
				knn.m_kNeighbours = j + 1;

				knn.m_weighting = WeightingScheme.Uniform;
				uniform[i][j] = knn.crossValidationError(data, 10);

				knn.m_weighting = WeightingScheme.Weighted;
				weighted[i][j] = knn.crossValidationError(data, 10);
			}
		}

		int bestP_uniform = findPValue(uniform);
		int bestK_uniform = findKValue(uniform);

		int bestP_weighted = findPValue(weighted);
		int bestK_weighted = findKValue(weighted);

		knn.m_weighting = findMinError(uniform) > findMinError(weighted) ? WeightingScheme.Weighted
				: WeightingScheme.Uniform;

		switch (knn.m_weighting) {
		case Weighted:
			knn.m_distance = bestP_weighted + 1;
			knn.m_kNeighbours = bestK_weighted + 1;
			break;

		case Uniform:
			knn.m_distance = bestP_uniform + 1;
			knn.m_kNeighbours = bestK_uniform + 1;
			break;
		}

		System.out.printf(
				"Cross validation error with K = %d, lp = %s, majority function = %s for auto_price data is: %f\n\n",
				knn.m_kNeighbours, (knn.m_distance == 4 ? "Infinity" : Integer.toString(knn.m_distance)),
				(knn.m_weighting == WeightingScheme.Weighted ? "weighted" : "uniform"),
				(knn.m_weighting == WeightingScheme.Weighted ? weighted[knn.m_distance - 1][knn.m_kNeighbours - 1]
						: uniform[knn.m_distance - 1][knn.m_kNeighbours - 1]));
	}
}
