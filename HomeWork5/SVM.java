package HomeWork5;

import weka.classifiers.functions.SMO;
import weka.classifiers.functions.supportVector.Kernel;
import weka.core.Instances;
import weka.core.Instance;

public class SVM {
	public SMO m_smo;

	public SVM() {
		this.m_smo = new SMO();
	}

	public void buildClassifier(Instances instances) throws Exception {
		m_smo.buildClassifier(instances);
	}

	public int[] calcConfusion(Instances instances) throws Exception {
		int[] confusion = new int[4];
		int tp = 0, fp = 0, tn = 0, fn = 0;

		double trueClass = 0d;
		double classification = 0d;

		for (Instance i : instances) {
			trueClass = i.classValue();
			classification = m_smo.classifyInstance(i);

			if (classification == trueClass) {
				tp += (classification == 1.0) ? 1 : 0;
				tn += (classification == 0.0) ? 1 : 0;
			} else {
				fp += (classification == 1.0) ? 1 : 0;
				fn += (classification == 0.0) ? 1 : 0;
			}
		}

		confusion[0] = tp;
		confusion[1] = fp;
		confusion[2] = tn;
		confusion[3] = fn;

		return confusion;
	}

	public void setKernel(Kernel kernel) {
		m_smo.setKernel(kernel);
	}

	public void setC(double c) {
		m_smo.setC(c);
	}

	public double getC() {
		return m_smo.getC();
	}
}